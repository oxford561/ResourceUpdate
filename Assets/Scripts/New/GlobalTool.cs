﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class GlobalTool
{
    public static byte[] GetBytes(string content)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(content);
        return bytes;
    }

    public static string GetNetworkState()
    {
        return Application.internetReachability.ToString();
    }

    public static bool IsNull(UnityEngine.Object target)
    {
        return target == null;
    }

    public static string GetPlatform()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        return "android";
#elif UNITY_IPHONE
        return "iOS";
#else
        return "editor";
#endif
    }

    public static bool IsNetError()
    {
        return Application.internetReachability == NetworkReachability.NotReachable;
    }

    //复制文件夹到目标路径
    public static void DirectoryCopy(string sourceDirectory, string targetDirectory)
    {
        if (!Directory.Exists(sourceDirectory) || !Directory.Exists(targetDirectory))
        {
            return;
        }
        try
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirectory);
            //获取目录下（不包含子目录）的文件和子目录
            FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();
            foreach (FileSystemInfo i in fileinfo)
            {
                if (i is DirectoryInfo)     //判断是否文件夹
                {
                    if (!Directory.Exists(targetDirectory + "/" + i.Name))
                    {
                        //目标目录下不存在此文件夹即创建子文件夹
                        Directory.CreateDirectory(targetDirectory + "/" + i.Name);
                    }
                    //递归调用复制子文件夹
                    DirectoryCopy(i.FullName, targetDirectory + "/" + i.Name);
                }
                else
                {
                    //不是文件夹即复制文件，true表示可以覆盖同名文件
                    File.Copy(i.FullName, targetDirectory + "/" + i.Name, true);
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log("复制文件出现异常");
        }
    }

    public static string GetFileNameWithoutExtension(string filePath)
    {
        return Path.GetFileNameWithoutExtension(filePath);
    }

    public static DirectoryInfo GetParent(string filePath)
    {
        return Directory.GetParent(filePath);
    }

    public static string[] ReadAllLines(string filePath)
    {
        return File.ReadAllLines(filePath);
    }

    public static void WriteAllLines(string filePath, string[] lines)
    {
        File.WriteAllLines(filePath, lines);
    }

    public static string ReadAllText(string filePath)
    {
        return File.ReadAllText(filePath);
    }

    public static UnityWebRequest UnityWebRequestTextureNew(string url)
	{
		return UnityWebRequestTexture.GetTexture(url);
	}

    public static Texture2D UnityWebRequestTextureGet(UnityWebRequest request)
	{
		return DownloadHandlerTexture.GetContent(request);
	}

    public static Sprite SpriteCreate(Texture2D tex)
	{
		return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
	}
}