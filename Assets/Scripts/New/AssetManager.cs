﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;

public class AssetManager : SingletonMono<AssetManager>
{

    public void Init(Action onComplete)
    {
        DownloadManager.Instance.Init();

        // 首先看看本地存在版本文件吗
        if (!File.Exists(DownloadManager.Instance.LocalVersionFilePath))
        {
            Debug.Log("本地没有资源，需要下载资源");
            DownloadManager.Instance.DownloadAndSaveVersionFile(() =>
            {
                //下载完 版本文件 开始下载 Assetbundles
                DownloadManager.Instance.DownloadMainAssetBundleFile(() =>
                {
                    // 下载完 主AssetBundle 后开始批量下载 所有的AssetBundle
                    AssetBundle mainAssetBundle = AssetBundle.LoadFromFile(DownloadManager.Instance.LocalMainAssetBundlePath);
                    if (mainAssetBundle == null)
                        return;
                    //获取AssetBundleManifest文件
                    AssetBundleManifest manifest = mainAssetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                    //获取AssetBundleManifest中的所有AssetBundle的名称信息
                    string[] assets = manifest.GetAllAssetBundles();
                    StartCoroutine(DownloadManager.Instance.DownloadFiles(assets, (isSuccess) =>
                    {
                        onComplete?.Invoke();
                    }));
                });
            });

        }
        else
        {
            DownloadFiles(onComplete);
        }
    }

    // 获取下载的文件列表(列表只包含下载路径)
    public List<string> GetDownloadFiles(List<AssetBundleInfo> fileList)
    {
        List<string> pathList = new List<string>();
        foreach (var file in fileList)
        {
            pathList.Add(file.FullName);
        }
        return pathList;
    }

    // 下载文件并且保存文件的版本信息到对应的版本文件中
    private void DownloadFilesAndSave(List<AssetBundleInfo> downloadFilesList)
    {
        if (downloadFilesList != null && downloadFilesList.Count >= 1)
        {
            foreach (var info in downloadFilesList)
            {
                StartCoroutine(
                    DownloadManager.Instance.DownloadSingleFile(info.FullName.Replace("/", ""), (state, handler) =>
                    {
                        // 下载成功后开始保存文件到本地
                        if (state)
                        {
                            DownloadManager.Instance.CreateFileByBytes(info.FullName.Replace("/", ""), handler.data, null);
                        }
                    })
                );
            }
        }
    }

    // 下载更新资源
    private void DownloadFiles(Action onComplete)
    {
        //1、获取本地的版本文件
        List<AssetBundleInfo> localAssetBundleInfos = DownloadManager.Instance.GetLocalVersionFileInfo();

        //2、获取网络上的版本文件
        DownloadManager.Instance.GetNetworkVersionFileInfo((networkAssetBundleInfos, data) =>
        {
            // 对比远程和本地的版本文件差异
            // 如果远程资源小于本地资源 可以进行删除操作
            DeleteLocalFiles(networkAssetBundleInfos, localAssetBundleInfos);

            // 查找需要更新的资源
            List<AssetBundleInfo> updateAssetBundleInfos = GetUpdateFiles(networkAssetBundleInfos, localAssetBundleInfos);

            //3、下载需要更新的列表
            if (updateAssetBundleInfos.Count >= 1)
            {
                // 需要更新下载
                DownloadFilesAndSave(updateAssetBundleInfos);

                Debug.Log("更新资源成功 开始加载 ");
            }
            else
            {
                Debug.Log("不需要更新资源，可以直接加载资源");
            }

            // 同步版本文件
            DownloadManager.Instance.CreateFileByBytes(DownloadManager.versionFileName, data, () =>
            {
                onComplete?.Invoke();
            });

        });
    }

    private List<AssetBundleInfo> GetUpdateFiles(List<AssetBundleInfo> networkAssetBundleInfos, List<AssetBundleInfo> localAssetBundleInfos)
    {
        List<AssetBundleInfo> updateAssetBundleInfos = new List<AssetBundleInfo>();
        Debug.Log("查找需要更新的 AssetBundle");
        for (int i = 0; i < networkAssetBundleInfos.Count; i++)
        {
            // 判断当前文件 本地是否存在
            AssetBundleInfo info = GetExistLocal(networkAssetBundleInfos[i].FullName, localAssetBundleInfos);
            if (info != null)
            {
                // 存在就进行下一步判断，判断文件是否一致
                if (!info.MD5.Equals(networkAssetBundleInfos[i].MD5))
                {
                    //文件是不一致的
                    networkAssetBundleInfos.Add(networkAssetBundleInfos[i]);
                    Debug.Log("本地文件不一致 " + networkAssetBundleInfos[i].FullName);

                    // 删除本地文件
                    File.Delete(DownloadManager.Instance.LocalUrlRootFolder + networkAssetBundleInfos[i].FullName);
                }
            }
            else
            {
                // 本地不存在当前文件 则加入更新列表中
                updateAssetBundleInfos.Add(networkAssetBundleInfos[i]);
                Debug.Log("本地文件不存在 " + networkAssetBundleInfos[i].FullName);
            }
        }

        return updateAssetBundleInfos;
    }

    // 删除 本地无用的文件
    private void DeleteLocalFiles(List<AssetBundleInfo> networkAssetBundleInfos, List<AssetBundleInfo> localAssetBundleInfos)
    {
        if (networkAssetBundleInfos.Count < localAssetBundleInfos.Count)
        {
            Debug.Log("看看要不要删除本地部分 AssetBundle 文件");
            // 如果远程资源数目小于本地资源（服务器可能删除了部分资源）
            // 除了需要下载更新的，还需要去除本地的资源
            for (int i = 0; i < localAssetBundleInfos.Count; i++)
            {
                AssetBundleInfo info = GetExistServer(localAssetBundleInfos[i].FullName, networkAssetBundleInfos);
                if (info == null)
                {
                    Debug.Log("开始删除资源 " + localAssetBundleInfos[i].FullName);
                    //删除本地资源
                    File.Delete(DownloadManager.Instance.LocalUrlRootFolder + localAssetBundleInfos[i].FullName);
                }
            }
        }
    }

    /// <summary>
    /// 判断本地资源是否是旧资源需要删除
    /// </summary>
    /// <param name="FullName"></param>
    /// <returns></returns>
    AssetBundleInfo GetExistServer(string FullName, List<AssetBundleInfo> networkAssetBundleInfos)
    {
        for (int i = 0; i < networkAssetBundleInfos.Count; i++)
        {
            if (FullName.Equals(networkAssetBundleInfos[i].FullName))
                return networkAssetBundleInfos[i];
        }
        return null;
    }

    /// <summary>
    /// 判断网络资源是否存在本地
    /// </summary>
    /// <param name="FullName"></param>
    /// <returns></returns>
    AssetBundleInfo GetExistLocal(string FullName, List<AssetBundleInfo> localAssetBundleInfos)
    {
        for (int i = 0; i < localAssetBundleInfos.Count; i++)
        {
            if (FullName.Equals(localAssetBundleInfos[i].FullName))
                return localAssetBundleInfos[i];
        }
        return null;
    }
}
