﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Text;

public class GlobalUtil
{
    private string platform = "Window";
    private string downloadUrl;
    private string versionUrlRootFolder = "";
    private string assetUrlRootFolder = "";
    private string versionFileName = "";

    public void CreateEmptyFile(string filePath)
    {
        string sFolder = GlobalTool.GetParent(filePath).ToString();
        if (!Directory.Exists(sFolder))
        {
            Directory.CreateDirectory(sFolder);
        }

        if (!File.Exists(filePath))
        {
            StreamWriter sw = new StreamWriter(filePath, false, Encoding.UTF8);
            sw.Close();
        }
    }

    public string GetPlatform()
    {
        return GlobalTool.GetPlatform();
    }
}
