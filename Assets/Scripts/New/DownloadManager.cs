﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System;

public class DownloadManager : SingletonMono<DownloadManager>
{
    public const string downloadUrl = "http://10.250.231.152/Window/";
    public const string versionFileName = "VersionFile.txt";
    public string MainAssetBundleName = "Window";// 由于测试，目前针对window平台，所以名称暂定
    public string LocalUrlRootFolder = "";
    public string LocalVersionFilePath = "";
    public string LocalMainAssetBundlePath = "";
    public void Init()
    {
        LocalUrlRootFolder = Application.persistentDataPath + "/Window/";//本地资源路径
        LocalVersionFilePath = LocalUrlRootFolder + versionFileName;
        LocalMainAssetBundlePath = LocalUrlRootFolder + MainAssetBundleName;
    }

    IEnumerator GetDownloadFilesText(string fileRelativePath, Action<string> callback)
    {
        // 断网检测
        if (GlobalTool.IsNetError())
        {
            Debug.Log("你网络不稳定");
            yield break;
        }

        string url = LocalUrlRootFolder + fileRelativePath;

        Debug.Log("GetDownloadFilesText url= " + url);

        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                UnityEngine.Debug.Log("Download Url " + url + "下载失败");
                yield break;
            }

            callback?.Invoke(www.downloadHandler.text);
        }
    }

    // 下载单个文件
    public IEnumerator DownloadSingleFile(string fileRelativePath, Action<bool, DownloadHandler> callback)
    {
        // 断网检测
        if (GlobalTool.IsNetError())
        {
            Debug.Log("你网络不稳定");
            yield break;
        }
        string url = downloadUrl + fileRelativePath;
        Debug.Log("=====DownloadFiles url=" + url);

        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                UnityEngine.Debug.Log("Download Url " + url + "下载失败");
                callback?.Invoke(false, null);
                yield break;
            }

            callback?.Invoke(true, www.downloadHandler);
        }
    }

    // 下载保存版本文件
    public void DownloadAndSaveVersionFile(Action onComplete)
    {
        StartCoroutine(DownloadSingleFile(versionFileName, (isSuccess, handler) =>
        {
            if (isSuccess)
            {
                CreateFileByBytes(versionFileName, handler.data, onComplete);
            }
        }));
    }

    // 下载 主 Assetbundle 文件
    public void DownloadMainAssetBundleFile(Action onComplete)
    {
        StartCoroutine(DownloadSingleFile(MainAssetBundleName, (isSuccess, handler) =>
       {
           if (isSuccess)
           {
               // 下载完 主AssetBundle 直接保存本地
               CreateFileByBytes(MainAssetBundleName, handler.data, onComplete);
           }
       }));
    }

    /* 下载文件清单
        文件清单为相对路径集合
    */
    public IEnumerator DownloadFiles(string[] arr, Action<bool> callback)
    {
        // 断网检测
        if (GlobalTool.IsNetError())
        {
            Debug.Log("你网络不稳定");
            yield break;
        }

        for (int i = 0; i < arr.Length; i++)
        {
            string url = downloadUrl + arr[i];// 对相对路径进行整合，后面可以根据相对路径下载保存到本地路径
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    UnityEngine.Debug.Log("Download Url " + url + "下载失败");
                    callback?.Invoke(false);
                    yield break;
                }
                CreateFileByBytes(arr[i], www.downloadHandler.data, null);
            }
        }

        callback?.Invoke(true);
    }

    public void CreateFileByBytes(string fileName, byte[] bytes, Action saveLocalComplate = null)
    {
        string toPath = LocalUrlRootFolder + fileName;
        int lastIndexOf = toPath.LastIndexOf(@"/");
        if (lastIndexOf != -1)
        {
            string localPath = toPath.Substring(0, lastIndexOf);//出去文件名以外的路径

            // 创建需要的目录结构
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }

        }

        using (FileStream fs = File.Create(toPath, bytes.Length))
        {
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();

            if (saveLocalComplate != null)
            {
                saveLocalComplate();
            }

            Debug.Log("已保存本地的文件 " + toPath);
        }
    }

    // 获取本地版本文件信息
    public List<AssetBundleInfo> GetLocalVersionFileInfo()
    {
        List<AssetBundleInfo> localAssetBundleInfos = new List<AssetBundleInfo>();
        StreamReader sr = new StreamReader(LocalUrlRootFolder + versionFileName);
        while (!sr.EndOfStream)
        {
            string line = sr.ReadLine().Replace("#", "").Trim();
            string[] infos = line.Split('|');
            localAssetBundleInfos.Add(new AssetBundleInfo() { FullName = infos[0], MD5 = infos[1] });
        }

        sr.Close();

        return localAssetBundleInfos;
    }

    // 获取网络版本文件信息
    public void GetNetworkVersionFileInfo(Action<List<AssetBundleInfo>, byte[]> callback)
    {
        List<AssetBundleInfo> assetBundleInfos = new List<AssetBundleInfo>();

        StartCoroutine(
            DownloadSingleFile(versionFileName, (isSuccess, handler) =>
            {
                if (isSuccess)
                {
                    // 下载成功后 不需要保存本地，直接读取下载到的版本文件数据
                    string[] files = handler.text.Split('#');
                    for (int i = 0; i <= files.Length - 1; i++)
                    {
                        string[] infos = files[i].Replace("\n", "").Replace("\r", "").Split('|');
                        assetBundleInfos.Add(new AssetBundleInfo() { FullName = infos[0], MD5 = infos[1], Size = int.Parse(infos[2]) });
                    }

                    callback?.Invoke(assetBundleInfos, handler.data);
                }
            })
        );
    }



}
