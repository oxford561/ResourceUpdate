﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Load : MonoBehaviour
{

    // 打包的Assetbundle 地址
    private string assetBundlePath;

    // 依赖文件配置
    private AssetBundleManifest m_Manifest;


    void Start()
    {
        // assetBundlePath = Application.dataPath + "/../MyAssetBundles/Window/";

        // //1、先加载依赖文件配置
        // AssetBundle manifestBundel = null;
        // m_Manifest = LoadAsset<AssetBundleManifest>("Window", "AssetBundleManifest", ref manifestBundel);



        // // 2、加载依赖项
        // string assetBundleName = "download/prefabs/myprefab.assetbundle";
        // string[] arrDps = m_Manifest.GetAllDependencies(assetBundleName);
        // AssetBundle[] arrDpsBundle = new AssetBundle[arrDps.Length];
        // for (int i = 0; i < arrDps.Length; i++)
        // {
        //     LoadAsset<Object>(arrDps[i], arrDps[i], ref arrDpsBundle[i]);
        // }

        // // 加载预设
        // AssetBundle myPrefabBundle = null;
        // GameObject go = LoadAsset<GameObject>(assetBundleName, "MyPrefab", ref myPrefabBundle);
        // Instantiate(go);


        // // 卸载资源

        // for (int i = 0; i < arrDpsBundle.Length; i++)
        // {
        //     arrDpsBundle[i].Unload(false);
        // }

        // myPrefabBundle.Unload(false);

        // manifestBundel.Unload(false);

        // DownloadMgr.Instance.Init();

        AssetManager.Instance.Init(()=>{Debug.Log("操作完成可进行下一步");});

    }

    public T LoadAsset<T>(string packageName, string assetName, ref AssetBundle bundle) where T : UnityEngine.Object
    {
        bundle = AssetBundle.LoadFromFile(string.Format("{0}{1}", assetBundlePath, packageName));
        return bundle.LoadAsset(assetName) as T;
    }


}
