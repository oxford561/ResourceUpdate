﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class DownloadTool : SingletonMono<DownloadMgr>
{


    // 下载 文件 到本地
    public IEnumerator DownloadFileAndSave(string url, string name, Action saveLocalComplate = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url + name))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                UnityEngine.Debug.Log("下载失败");
                yield break;
            }
            byte[] datas = www.downloadHandler.data;
            SaveAssetBundle(name, datas, saveLocalComplate);
        };
    }

    // 解压 AssetBundle 到本地
    void SaveAssetBundle(string toPath, byte[] bytes, Action saveLocalComplate = null)
    {
        // string toPath = LocalFileFullName + fileName;
        int lastIndexOf = toPath.LastIndexOf(@"/");
        if (lastIndexOf != -1)
        {
            string localPath = toPath.Substring(0, lastIndexOf);//出去文件名以外的路径

            // 创建需要的目录结构
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }
        }

        using (FileStream fs = File.Create(toPath, bytes.Length))
        {
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
        }

        if (saveLocalComplate != null)
        {
            saveLocalComplate();
        }
    }
}
