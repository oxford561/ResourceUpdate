﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 下载数据的实体
public class AssetBundleInfo
{
    // 资源的名称
    public string FullName;

    // MD5
    public string MD5;

    // 文件大小 （K）
    public int Size;
}
