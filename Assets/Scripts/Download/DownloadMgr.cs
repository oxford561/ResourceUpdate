﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using UnityEngine.Networking;

public class DownloadMgr : SingletonMono<DownloadMgr>
{
    private const string DownloadBasUrl = "http://10.250.231.152/";
    //根目录的文件夹产生的AssetBundles文件地址
    private string MainAssetBundleName = "Window";
    private const string VersionFileName = "VersionFile.txt";
    private List<AssetBundleInfo> assetBundleInfos;
    private List<AssetBundleInfo> localAssetBundleInfos;
    private List<AssetBundleInfo> updateAssetBundleInfos;

    public const string DownloadUrl = DownloadBasUrl + "Window/";

    public string LocalFileFullName = "";//本地资源路径

    public Action downloadFinish;

    public void Init()
    {
        LocalFileFullName = Application.persistentDataPath + "/Window/";//本地资源路径

        assetBundleInfos = new List<AssetBundleInfo>();
        localAssetBundleInfos = new List<AssetBundleInfo>();
        updateAssetBundleInfos = new List<AssetBundleInfo>();

        Debug.Log("资源检测开始");
        // 首先看看本地存在版本文件吗
        if (!File.Exists(LocalFileFullName + VersionFileName))
        {
            Debug.Log("本地没有资源，需要下载资源");
            // 先下载版本文件
            StartCoroutine(DownloadVersionFile((datas) =>
            {
                //下载完 版本文件 开始下载 Assetbundles
                StartCoroutine(DownloadAssetBundles(() =>
                {
                    Debug.Log("资源下载成功");

                }));
            }, true));
        }
        else
        {
            Debug.Log("本地存在资源");
            // 收集本地的 AssetBundle 信息
            Debug.Log("收集本地 AssetBundle 信息");
            GetLocalAssetBundleInfo();

            // 首先拿到服务器上的版本文件与本地的进行对比，相同则本地加载，不相同则更新下载然后加载
            // 先下载版本文件
            StartCoroutine(DownloadVersionFile((datas) =>
            {
                // 收集 服务器上的 AssetBundle 信息,由于最后会出现 # 标识符，这里的长度-1
                Debug.Log("收集远程 AssetBundle 信息");
                string[] files = datas.Split('#');
                for (int i = 0; i <= files.Length - 1; i++)
                {
                    string[] infos = files[i].Replace("\n", "").Replace("\r", "").Split('|');
                    assetBundleInfos.Add(new AssetBundleInfo() { FullName = infos[0], MD5 = infos[1], Size = int.Parse(infos[2]) });
                }

                // 对比远程和本地的版本文件差异
                // 如果远程资源小于本地资源
                if (assetBundleInfos.Count < localAssetBundleInfos.Count)
                {
                    Debug.Log("看看要不要删除本地部分 AssetBundle 文件");
                    // 如果远程资源数目小于本地资源（服务器可能删除了部分资源）
                    // 除了需要下载更新的，还需要去除本地的资源
                    for (int i = 0; i < localAssetBundleInfos.Count; i++)
                    {
                        AssetBundleInfo info = GetExistServer(localAssetBundleInfos[i].FullName);
                        if (info == null)
                        {
                            Debug.Log("开始删除资源 "+LocalFileFullName + localAssetBundleInfos[i].FullName);
                            //删除本地资源
                            File.Delete(LocalFileFullName + localAssetBundleInfos[i].FullName);
                        }
                    }
                }

                // 还是查找需要更新的资源
                Debug.Log("查找需要更新的 AssetBundle");
                for (int i = 0; i < assetBundleInfos.Count; i++)
                {
                    // 判断当前文件 本地是否存在
                    AssetBundleInfo info = GetExistLocal(assetBundleInfos[i].FullName);
                    if (info != null)
                    {
                        // 存在就进行下一步判断，判断文件是否一致
                        if (!info.MD5.Equals(assetBundleInfos[i].MD5))
                        {
                            //文件是不一致的
                            updateAssetBundleInfos.Add(assetBundleInfos[i]);
                            Debug.Log("本地文件不一致 " + assetBundleInfos[i].FullName);

                            // 删除本地文件
                            File.Delete(LocalFileFullName + assetBundleInfos[i].FullName);
                        }
                    }
                    else
                    {
                        // 本地不存在当前文件 则加入更新列表中
                        updateAssetBundleInfos.Add(assetBundleInfos[i]);
                        Debug.Log("本地文件不存在 " + assetBundleInfos[i].FullName);
                    }
                }

                // 下载需要更新的列表
                if (updateAssetBundleInfos.Count >= 1)
                {
                    // 需要更新下载
                    Debug.Log("需要更新资源");
                    for (int i = 0; i < updateAssetBundleInfos.Count; i++)
                    {
                        StartCoroutine(DownloadAssetBundleAndSave(DownloadUrl, updateAssetBundleInfos[i].FullName.Replace("/", ""), () =>
                        {

                        }));
                    }

                    Debug.Log("更新资源成功 开始加载 ");
                }
                else
                {
                    Debug.Log("不需要更新资源，直接加载资源");
                }

            }, true));
        }
    }

    /// <summary>
    /// 从 AssetBundle 中加载 场景
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadSceneBundle(string sceneName, Action callback)
    {
        StartCoroutine(LoadFileAsync(LocalFileFullName + sceneName + ".assetbundle", () =>
        {
            callback?.Invoke();
        }));
    }

    IEnumerator LoadFileAsync(string FullName, Action callBack)
    {
        yield return new WaitForSeconds(.5f);
        AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(FullName);
        yield return request;

        if (request.isDone)
        {
            AssetBundle ab = request.assetBundle;

            callBack?.Invoke();
        }
    }

    #region 版本文件操作

    

    /// <summary>
    /// 判断网络资源是否存在本地
    /// </summary>
    /// <param name="FullName"></param>
    /// <returns></returns>
    AssetBundleInfo GetExistLocal(string FullName)
    {
        for (int i = 0; i < localAssetBundleInfos.Count; i++)
        {
            if (FullName.Equals(localAssetBundleInfos[i].FullName))
                return localAssetBundleInfos[i];
        }
        return null;
    }

    /// <summary>
    /// 判断本地资源是否是旧资源需要删除
    /// </summary>
    /// <param name="FullName"></param>
    /// <returns></returns>
    AssetBundleInfo GetExistServer(string FullName)
    {
        for (int i = 0; i < assetBundleInfos.Count; i++)
        {
            if (FullName.Equals(assetBundleInfos[i].FullName))
                return assetBundleInfos[i];
        }
        return null;
    }

    // 收集本地 AssetBundle 信息
    void GetLocalAssetBundleInfo()
    {
        StreamReader sr = new StreamReader(LocalFileFullName + VersionFileName);
        while (!sr.EndOfStream)
        {
            string line = sr.ReadLine().Replace("#", "").Trim();
            string[] infos = line.Split('|');
            localAssetBundleInfos.Add(new AssetBundleInfo() { FullName = infos[0], MD5 = infos[1] });
        }

        sr.Close();
    }

    #endregion


    #region 下载加载资源

    /// <summary>
    /// 下载版本文件
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    IEnumerator DownloadVersionFile(Action<string> callback, bool isCache = false)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(DownloadUrl + VersionFileName))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                UnityEngine.Debug.Log("下载失败");
                yield break;
            }

            string data = www.downloadHandler.text;
            byte[] datas = www.downloadHandler.data;
            callback?.Invoke(data);
            if (isCache)
                SaveAssetBundle(VersionFileName, datas);
        }
    }

    /// <summary>
    /// 下载根目录AssetBundle文件
    /// </summary>
    /// <returns></returns>
    IEnumerator DownloadAssetBundles(Action callback = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(DownloadUrl + MainAssetBundleName))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                UnityEngine.Debug.Log("下载失败");
                yield break;
            }

            byte[] datas = www.downloadHandler.data;
            SaveAssetBundle(MainAssetBundleName, datas);
            string localFullName = LocalFileFullName + MainAssetBundleName;
            AssetBundle mainAssetBundle = AssetBundle.LoadFromFile(localFullName);
            if (mainAssetBundle == null)
                yield break;
            //获取AssetBundleManifest文件
            AssetBundleManifest manifest = mainAssetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            //获取AssetBundleManifest中的所有AssetBundle的名称信息
            string[] assets = manifest.GetAllAssetBundles();
            for (int i = 0; i < assets.Length; i++)
            {
                //开启协程下载所有的
                StartCoroutine(DownloadAssetBundleAndSave(DownloadUrl, assets[i], () =>
                {
                    //下载完成，按照之前的方法，从本地加载AssetBundle并设置。
                    callback?.Invoke();
                }));
            }
        }
    }

    IEnumerator DownloadAssetBundleAndSave(string url, string name, Action saveLocalComplate = null)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url + name))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                UnityEngine.Debug.Log("下载失败12");
                yield break;
            }
            byte[] datas = www.downloadHandler.data;
            SaveAssetBundle(name, datas, saveLocalComplate);
        };
    }

    void SaveAssetBundle(string fileName, byte[] bytes, Action saveLocalComplate = null)
    {
        string toPath = LocalFileFullName + fileName;
        int lastIndexOf = toPath.LastIndexOf(@"/");
        if (lastIndexOf != -1)
        {
            string localPath = toPath.Substring(0, lastIndexOf);//出去文件名以外的路径

            // 创建需要的目录结构
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }

        }

        using (FileStream fs = File.Create(toPath, bytes.Length))
        {
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
        }

        if (saveLocalComplate != null)
        {
            saveLocalComplate();
        }
    }

    #endregion

}
