﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Text;

public class CreateAssetBundle
{
    [MenuItem("Tools/资源打包")]
    static void BuildAsset()
    {
        string path = Application.dataPath + "/../MyAssetBundles/Window";

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }

     [MenuItem("Tools/打开持久化目录")]
    static void OpenPersistentPath()
    {
        string path = Application.persistentDataPath;

        EditorUtility.RevealInFinder(path);
    }

    [MenuItem("Tools/创建版本文件")]
    static void CreateVersionFile()
    {
        string path = Application.dataPath + "/../MyAssetBundles/" + "Window";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        string strVersionFilePath = path + "/VersionFile.txt";

        // 如果版本文件存在，则删除
        FileUtil.DeleteFile(strVersionFilePath);

        StringBuilder sbContent = new StringBuilder();

        DirectoryInfo dirctory = new DirectoryInfo(path);
        //拿到文件夹下所有文件
        FileInfo[] arrFiles = dirctory.GetFiles("*", SearchOption.AllDirectories);
        for (int i = 0; i < arrFiles.Length; i++)
        {
            FileInfo file = arrFiles[i];
            string fullName = file.FullName;//全名 包括路径扩展名
            //相对路径
            string name = fullName.Substring(fullName.IndexOf("Window") + "Window".Length + 1);

            string md5 = FileUtil.GetFileMD5(fullName);//文件的MD5
            if (md5 == null) continue;

            string size = Math.Ceiling(file.Length / 1024f).ToString();//文件大小
            string strLine = string.Empty;
            if (i == arrFiles.Length - 1)
            {
                strLine = string.Format("{0}|{1}|{2}", name, md5, size);
            }
            else
            {
                strLine = string.Format("{0}|{1}|{2}#", name, md5, size);
            }
            Debug.Log(strLine);
            sbContent.AppendLine(strLine);
        }

        FileUtil.CreateTextFile(strVersionFilePath, sbContent.ToString());
        Debug.Log("生成版本文件");
    }
}
